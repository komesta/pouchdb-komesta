'use strict';

/*eslint-env node */

module.exports = function(config) {
    config.set({
        frameworks: ['jasmine'],
        files: [
            'bower_components/angular/angular.js',
            'bower_components/pouchdb/dist/pouchdb.js',
            'bower_components/pouchdb-find/dist/pouchdb.find.js',
            'node_modules/angular-mocks/angular-mocks.js',
            'pouchdb-komesta.js',
            'test/*.js'
        ],
        browsers: [
            'PhantomJS',
            'Firefox'
        ],
        autoWatch: false,
        singleRun: true,
        reporters: [
            'progress',
        ]
    });
};
