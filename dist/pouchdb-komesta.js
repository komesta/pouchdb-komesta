'use strict';

var pouchDBExceptions = {
    CREATE_ERR: 0,
    SAVING_ERROR: 1,
    DOCUMENT_CONFLICT: 2,
    INDEX_ADD_ERROR: 3,
    GET_INDEXES_EXCEPTION: 4,
    FIND_EXCEPTION: 5,
    NO_DATABASE: 6
};

/**
 * @ngdoc service
 * @name pouchDBService
 * @description
 * # pouchDBService
 */
angular.module('pouchdbk', [])
    .service('pouchDBService', function ($q, $log) {
        var database;
        var changeListener;
        var listeners = [];

        /**
         * Prida index do databaze
         *
         * @param index
         * @returns Promise
         * @reject s {type: pouchDBExceptions.INDEX_ADD_ERROR, message: 'Cannot create index', cause: err}
         * @reject s {type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: err}
         */
        function _addIndexToDB(index) {
            if(!database)
                return $q.reject({type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: null});

            return $q.when(database.createIndex({index: {fields: [index]}}))
                .catch(function (error) {
                    throw {type: pouchDBExceptions.INDEX_ADD_ERROR, message: 'Cannot create index', cause: error}
                })

        }

        /**
         *Adds index on fields to db
         * @param fields {Array}
         * @private
         * @reject s {type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: err}
         */
        function _addMultipleFieldIndex(fields) {
            if(!database)
                return $q.reject({type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: null});

            return $q.when(
                database.createIndex({index: {fields: fields}})
                    .catch(function (error) {
                        throw {type: pouchDBExceptions.INDEX_ADD_ERROR, message: 'Cannot create index', cause: error}
                    })
            );
        }


        /**
         * Vrati seznam indexu
         * @return Promise
         * @resolve se seznamem indexu
         * @reject s {type: pouchDBExceptions.GET_INDEXES_EXCEPTION, message: 'Cannot get indexes', cause: err}
         * @reject s {type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: err}
         * @private
         */
        function _listIndexes() {
            if(!database)
                return $q.reject({type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: null});

            return $q.when(database.getIndexes());
        }

        /**
         * Vytvoreni databaze. pokud je jmeno ve formatu http://xxx.xxx.xx/dbname - pripoji se ke vzdalene databazi.
         * Pri pripojeni ke vzdalene databazi se pouzije zadane jmeno a heslo.
         * @param databaseName
         * @param user
         * @param pass
         * @returns Promise
         * @rejection {type: pouchDBExceptions.CREATE_ERR, message: 'Cannot create database', cause: xxx}
         */
        function _setDatabase(databaseName, user, pass) {

            if(!user || !pass){
                database = new PouchDB(databaseName, {
                    revs_limit: 1,
                    auto_compaction: true,
                    skip_setup: true
                });
            }else {
                database = new PouchDB(databaseName, {
                    revs_limit: 1,
                    auto_compaction: true,
                    skip_setup: true,
                    auth: {username: user, password: pass}
                });
            }

            return $q.when(database.info()
                .then(function () {
                    $log.debug('creation of database done.');
                    return database;
                })
                .catch(function (err) {
                    $log.debug('creation failed');
                    database = null;
                    throw {
                        type: pouchDBExceptions.CREATE_ERR,
                        message: 'Cannot create database',
                        cause: err
                    };
                }));

        }

        function _changeDatabase(db){
            database = db;
            return database;
        }

        /**
         * zaregistruje listener, ktery je volan pri zmene dat v databazi
         * @param listener listener(changedDoc)
         */
        function _registerListener(listener) {
            if (listeners.length === 0) {
                _startListening();
            }

            listeners.push(listener);
        }

        function _startListening() {
            if(!database)
                return $q.reject({type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: null});

            $log.debug('Start listenig on db');
            changeListener = database.changes({
                live: true,
                include_docs: true
            }).on("change", function (change) {
                $log.debug('On change called on db', change);
                /*if(!change.deleted) {
                 $rootScope.$broadcast("$pouchDB:change", change);
                 } else {
                 $rootScope.$broadcast("$pouchDB:delete", change);
                 }*/
                angular.forEach(listeners, function (listener) {
                    listener(change);
                });
            });
        }

        function _stopListening() {
            changeListener.cancel();
        }

        function _sync(remoteDatabase) {
            if(!database)
                return $q.reject({type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: null});

            database.sync(remoteDatabase, {live: true, retry: true});
        }

        /**
         *
         * Ulozi dokument do pouchDB.
         *
         * @param jsonDocument
         * @return angular promise
         * @resolve {"ok" : true, "id" : "8A2C3761-FFD5-4770-9B8C-38C33CED300A", "rev" : "1-d3a8e0e5aa7c8fff0c376dac2d8a4007"}
         * @reject v pripade, ze se nepodari ulozit dokument {type: pouchDBExceptions.SAVING_ERROR, message: 'Cannot save document', cause: error};
         * @reject v pripade konfliktu revize {type: pouchDBExceptions.DOCUMENT_CONFLICT, message: 'Revision of document conflict', cause: error};
         * @reject s {type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: err}
         */
        function _saveDocument(jsonDocument) {
            if(!database)
                return $q.reject({type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: null});

            if (!jsonDocument._id) {
                $log.debug('saving document with no id');
                return $q.when(database.post(jsonDocument))
                    .catch(function (error) {
                        if (error.status && error.status === 409) {
                            throw {
                                type: pouchDBExceptions.DOCUMENT_CONFLICT,
                                message: 'Revision of document conflict',
                                cause: error
                            };
                        }
                        throw {type: pouchDBExceptions.SAVING_ERROR, message: 'Cannot save document', cause: error};
                    });
            } else {
                $log.debug('saving document with id:', jsonDocument._id);
                return $q.when(database.put(jsonDocument))
                    .catch(function (error) {
                        if (error.status && error.status === 409) {
                            throw {
                                type: pouchDBExceptions.DOCUMENT_CONFLICT,
                                message: 'Revision of document conflict',
                                cause: error
                            };
                        }
                        throw {type: pouchDBExceptions.SAVING_ERROR, message: 'Cannot save document', cause: error};
                    });
            }
        }

        function _deleteDocument(documentId, documentRevision) {
            if(!database)
                return $q.reject({type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: null});

            return database.remove(documentId, documentRevision);
        }

        /**
         * Vrati dokument z databaze
         * @param documentId
         * @return Promise
         * @resolve s dokumentem
         * @reject s {status: 404} pokud dokument neexistuje
         * @reject s jinym statusem
         * @reject s {type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: err}
         */
        function _getDocument(documentId) {
            if(!database)
                return $q.reject({type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: null});

            return database.get(documentId);
        }

        function _destroy() {
            if(!database)
                return $q.reject({type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: null});

            $log.debug('Destroying database');
            return database.destroy();
        }


        /**
         * Slouzi k hledani dokumentu obsahujicich property s uvedenou hodnotou.
         *
         * @param property kterou maji hledane dokumenty
         * @param value hodnota teto property
         * @return angular promise
         * @resolve [] dokumentu nebo prazdne pole, pokud zadne dokumenty neodpovidaji
         * @rejection {type: pouchDBExceptions.FIND_EXCEPTION, message: 'Exception when finding documents', cause: xxx}
         * @reject s {type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: err}
         */
        function _find(property, value) {
            if(!database)
                return $q.reject({type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: null});

            var selector = {};
            selector[property] = value;
            var query = {selector: selector};

            return $q.when(database.find(query))
                .then(function (data) {
                    if (data && data.warning)
                        $log.warn(data.warning);
                    return data;
                })
                .catch(function (error) {
                    throw {
                        type: pouchDBExceptions.FIND_EXCEPTION,
                        message: 'Exception when finding documents',
                        cause: error
                    };
                });
        }

        /**
         * Vyhleda v databazi dokumenty podle zadane query
         * @param query
         * @return Promise
         * @resolve [] dokumentu nebo prazdne pole, pokud zadne dokumenty neodpovidaji
         * @rejection {type: pouchDBExceptions.FIND_EXCEPTION, message: 'Exception when finding documents', cause: xxx}
         * @reject s {type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: err}
         */
        function _customFind(query) {
            if(!database)
                return $q.reject({type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: null});

            return $q.when(database.find(query))
                .then(function (data) {
                    if (data && data.warning)
                        $log.warn(data.warning);
                    return data;
                })
                .catch(function (error) {
                    throw {
                        type: pouchDBExceptions.FIND_EXCEPTION,
                        message: 'Exception when finding documents',
                        cause: error
                    };
                });
        }

        /**
         * Vrati vsechny dokumenty v databazi vcetne priloh
         * @return Promise
         * @resolve with  structure like this
         {
          "offset": 0,
          "total_rows": 1,
          "rows": [{
            "doc": {
              "_id": "0B3358C1-BA4B-4186-8795-9024203EB7DD",
              "_rev": "1-5782E71F1E4BF698FA3793D9D5A96393",
              "title": "Sound and Vision",
              "_attachments": {
                "attachment/its-id": {
                  "content_type": "image/jpg",
                  "data": "R0lGODlhAQABAIAAAP7//wAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==",
                  "digest": "md5-57e396baedfe1a034590339082b9abce"
                }
              }
            },
            "id": "0B3358C1-BA4B-4186-8795-9024203EB7DD",
            "key": "0B3358C1-BA4B-4186-8795-9024203EB7DD",
            "value": {
             "rev": "1-5782E71F1E4BF698FA3793D9D5A96393"
            }
          }]
         }
         * @rejection {type: pouchDBExceptions.FIND_EXCEPTION, message: 'Exception when finding documents', cause: xxx}
         * @reject s {type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: err}
         */
        function _getAllDocs() {
            if(!database)
                return $q.reject({type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: null});

            return $q.when(database.allDocs({
                include_docs: true,
                attachments: true
            }))
                .catch(function (error) {
                    throw {
                        type: pouchDBExceptions.FIND_EXCEPTION,
                        message: 'Exception when finding documents',
                        cause: error
                    };
                });


        }

        /**
         * Updatuje / vytvori dokumenty obsazene v poli arrayDocs. Neni transakcni, takze vrati pole s info o kazde polozce v poli.
         * Info je {ok: true, id: xxx, rev: xxx} nebo {status:xxx, name: xxx, mesasge: xxx, error: true}
         * @param arrayDocs
         * @return {Promise}
         */
        function _bulkDocs(arrayDocs) {
            return $q.when(database.bulkDocs(arrayDocs))
        }


        return {
            setDatabase: _setDatabase,
            changeDatabase: _changeDatabase,
            sync: _sync,
            saveDocument: _saveDocument,
            deleteDocument: _deleteDocument,
            getDocument: _getDocument,
            destroy: _destroy,
            registerListener: _registerListener,
            find: _find,
            addIndexToDB: _addIndexToDB,
            listIndexes: _listIndexes,
            customFind: _customFind,
            addMultipleFieldIndex: _addMultipleFieldIndex,
            getAllDocs: _getAllDocs,
            bulkDocs: _bulkDocs
        };
    });
