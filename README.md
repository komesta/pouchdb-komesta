##Usage
1. Add this to bower.json

    ```
    "pouchdb-komesta.js": "https://komesta@gitlab.com/komesta/pouchdb-komesta.git#^0.0.1"
    ```

2. Add `pouchdb` as a module dependency:

    ```js
    angular.module('app', ['pouchdbk']);
    ```

3. Inject the `pouchDB` service in your app and connect to database:

    ```js
    angular.service('service', function(pouchDBService) {
      pouchDBService.setDatabase('databaseNameOrURL');
    });
    ```