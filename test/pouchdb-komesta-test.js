'use strict';

var testDocId = 'testDoc';
var testDatabaseName = 'testDatabase';

describe('Service: pouchDBService', function () {
    var pouchDBService, $log, $q;
    var scope, $rootScope;

    // load the service's module
    beforeEach(module('pouchdbk'));

    beforeEach(inject(function ($injector) {
        pouchDBService = $injector.get('pouchDBService');
        $log = $injector.get('$log');
        $rootScope = $injector.get('$rootScope');
        scope = $injector.get('$rootScope').$new();
        $q = $injector.get('$q');

        setInterval(function () {
            $rootScope.$digest();
            //console.log('$digest');
        }, 100);
    }));

    beforeEach(function (done) {
        PouchDB.debug.enable('pouchdb:find');
        return pouchDBService.setDatabase(testDatabaseName)
            .then(function () {
                return pouchDBService.destroy();
            })
            .catch(function (err) {
                console.info(err);
                expect(true).toBe(false);
            })
            .finally(function () {
                done();
            });
    });

    afterEach(function () {
        console.log($log.debug.logs);
    });

    describe('pouchDBService destroy database function', function () {
        it('should destroy the database', function (done) {
            pouchDBService.setDatabase(testDatabaseName)
                .then(function (databaseInfo) {
                    return pouchDBService.destroy()
                })
                .then(function () {
                    done();
                })
        });
    });

    describe('pouchDBService setDatabase function', function () {
        it('should not set and reject the promise', function () {
            pouchDBService.setDatabase('http://nonexisting.domain.cz/nonexistingDatabase')
                .then(function (database) {
                    expect(false).toBe(true);
                })
                .catch(function (err) {
                    console.log(err);
                    expect(err.type).toBe(pouchDBExceptions.CREATE_ERR);
                })
                .finally(function () {
                    done();
                });
        });

        it('should create the database', function (done) {
            pouchDBService.setDatabase(testDatabaseName)
                .then(function (database) {
                    //console.info(database);
                    expect(database).toBeDefined();
                    expect(database.name).toBe(testDatabaseName);
                    expect(true).toBe(true);
                })
                .catch(function (err) {
                    //console.info('should create the database error', err);
                    expect(false).toBe(true);
                })
                .finally(function () {
                    done();
                });

        });


    });


    describe('saveDocument function', function () {
        it('should save document', function (done) {
            pouchDBService.setDatabase(testDatabaseName)
                .then(function () {
                    pouchDBService.saveDocument({title: 'new document'})
                        .then(function (savedDocumentInfo) {
                            expect(savedDocumentInfo.ok).toBe(true);
                        });
                })
                .finally(function () {
                    done();
                });
        });
        it('should failed with document conflict', function (done) {
            pouchDBService.setDatabase(testDatabaseName)
                .then(function () {
                    return pouchDBService.saveDocument({_id: 'test', title: 'new document'});
                })
                .then(function (firstDocumentInfo) {
                    expect(firstDocumentInfo.ok).toBe(true);
                    return firstDocumentInfo;
                })
                .then(function () {
                    return pouchDBService.saveDocument({_id: 'test', title: 'new document2'});
                })
                .then(function (secondDocumentInfo) {
                    expect(secondDocumentInfo.ok).toBe(true);
                })
                .catch(function (error) {
                    expect(error.type).toBe(pouchDBExceptions.DOCUMENT_CONFLICT);
                })
                .finally(function () {
                    done();
                });
        });


    });

    describe('pouchDBService registration of listener function', function () {
        it('should register listenner and call it on change.', function (done) {

            var testData = 'testData';
            var doc = {_id: testDocId, data: testData};
            var count = 0;

            pouchDBService.setDatabase(testDatabaseName)
                .then(function () {
                    //console.log('registering listener');
                    return pouchDBService.registerListener(function (data) {
                        // console.log(data);
                        if (data.id === testDocId) {
                            count++;
                        }
                    });
                })
                .then(function () {
                    doc.data = 'data2';
                    return pouchDBService.saveDocument(doc);
                })
                .catch(function (err) {
                    //console.log('pouchDBService registration of listener function: ', err);
                    expect(false).toBe(true);
                });

            setInterval(function () {
                //console.log('waiting for changes count: ' + count);
                if (count) {
                    done();
                }
            }, 100);

        });
    });
    //{index: {fields: [index]}}
    describe('create index in db', function () {
        it('should create index in db', function (done) {
            pouchDBService.setDatabase(testDatabaseName)
                .then(function () {
                    pouchDBService.addIndexToDB('testIndex')
                        .then(function () {
                            done();
                        })
                })

        });
        //@reject s {type: pouchDBExceptions.NO_DATABASE, message: 'Database is not connected', cause: err}
        it('should reject with {type: pouchDBExceptions.NO_DATABASE, message: \'Database is not connected\', cause: err}', function (done) {
            pouchDBService.setDatabase('http://nonexistingurl/nonexistingdomain.com')
                .then(function () {

                })
                .catch(function () {
                    pouchDBService.addIndexToDB('testIndex')
                        .catch(function (err) {
                            expect(err.type).toBe(pouchDBExceptions.NO_DATABASE);
                            done();
                        })
                })

        })
    });

    describe('Get indexes list', function () {
        it('should return indexes registered in db', function (done) {
            pouchDBService.setDatabase(testDatabaseName)
                .then(function () {
                    pouchDBService.listIndexes()
                        .then(function (data) {
                            console.log(data.indexes);
                            angular.forEach(data.indexes, function (index) {
                                console.log(index.name);
                            });
                            done();
                        })
                })

        })
    });

    describe('Insert index to database', function () {
        it('should insert index to database', function (done) {
            pouchDBService.setDatabase(testDatabaseName)
                .then(function () {
                    return pouchDBService.addIndexToDB('testIndex')
                })
                .then(function () {
                    pouchDBService.listIndexes()
                        .then(function (data) {
                            console.log(data.indexes);
                            expect(data.indexes.length).toBe(2);
                            done();
                        })
                })


        })
    });

    describe('Search by index', function () {
        it('Should search doc using index.', function (done) {

            var resource1 = {_id: 'testDocId1', data: 'testData1', type: 'resource'};
            var resource2 = {_id: 'testDocId2', data: 'testData2', type: 'resource'};
            var resource3 = {_id: 'testDocId3', data: 'testData3', type: 'resource'};
            var event1 = {_id: 'testDocId4', data: 'testData4', type: 'event'};
            var event2 = {_id: 'testDocId5', data: 'testData5', type: 'event'};
            var propertyName = 'type';

            pouchDBService.setDatabase(testDatabaseName)
                .then(function () {
                    return $q.all([pouchDBService.saveDocument(resource1), pouchDBService.saveDocument(resource2), pouchDBService.saveDocument(resource3),
                        pouchDBService.saveDocument(event1), pouchDBService.saveDocument(event2)]);
                })
                .then(function () {
                    return pouchDBService.find(propertyName, 'resource')
                        .then(function (data) {
                            console.log(data);
                            expect(!!data.warning).toBe(true);  //ma projit s warningem na neexistujici index
                            expect(data.docs.length).toBe(3);
                            angular.forEach(data.docs, function (doc) {
                                expect(doc.type === 'resource')
                            });
                        })
                        .catch(function (error) {
                            console.log(error);
                            expect(false).toBe(true);
                        });
                })
                .then(function () {
                    return pouchDBService.addIndexToDB(propertyName)
                })
                .then(function () {
                    pouchDBService.find(propertyName, 'event')
                        .then(function (data) {
                            console.log(data);
                            expect(!data.warning).toBe(true); //tady uz index existuje
                            expect(data.docs.length).toBe(2);
                            angular.forEach(data.docs, function (doc) {
                                expect(doc.type === 'event')
                            });
                            done();
                        })
                        .catch(function (error) {
                            console.log(error);
                            expect(false).toBe(true);
                        });
                })

        });
        it('Should return empty array.', function (done) {

            var resource1 = {_id: 'testDocId1', data: 'testData1', type: 'resource'};
            var resource2 = {_id: 'testDocId2', data: 'testData2', type: 'resource'};
            var resource3 = {_id: 'testDocId3', data: 'testData3', type: 'resource'};
            var event1 = {_id: 'testDocId4', data: 'testData4', type: 'event'};
            var event2 = {_id: 'testDocId5', data: 'testData5', type: 'event'};
            var propertyName = 'type';

            pouchDBService.setDatabase(testDatabaseName)
                .then(function () {
                    return pouchDBService.addIndexToDB(propertyName)
                })
                .then(function () {
                    return $q.all([pouchDBService.saveDocument(resource1), pouchDBService.saveDocument(resource2), pouchDBService.saveDocument(resource3),
                        pouchDBService.saveDocument(event1), pouchDBService.saveDocument(event2)]);
                })
                .then(function () {
                    return pouchDBService.find(propertyName, 'nonexistingType')
                        .then(function (data) {
                            console.log(data);
                            expect(!data.warning).toBe(true);
                            expect(data.docs.length).toBe(0);
                            done();
                        })
                        .catch(function (error) {
                            console.log(error);
                            expect(false).toBe(true);
                        });
                })


        });
    });

    describe('Search in newly created database', function () {
        it('Should return empty array.', function (done) {

            var propertyName = 'type';

            pouchDBService.setDatabase(testDatabaseName)
                .then(function () {
                    return pouchDBService.find(propertyName, 'resource')
                        .then(function (data) {
                            expect(!!data).toBe(true);
                            expect(!!data.warning).toBe(true);  //ma projit s warningem na neexistujici index
                            expect(data.docs.length).toBe(0);
                            done();
                        })
                        .catch(function (error) {
                            console.log(error);
                            expect(false).toBe(true);
                        });
                })
        });
    });

    describe('Get nonexisting document', function () {
        it('Should return null.', function (done) {

            var nonexistingId = 'nonexistingId';

            pouchDBService.setDatabase(testDatabaseName)
                .then(function () {
                    return pouchDBService.getDocument(nonexistingId)
                        .then(function (data) {
                            expect(true).toBe(false);
                        })
                        .catch(function (error) {
                            console.log(error);
                            expect(error.status).toBe(404);
                            done();
                        });
                })
        });
    });

    describe('_customFind Search in doc array for a value and return doc', function () {

        it('should return doc', function (done) {

            var document1 = {name: 'testDoc1', pole: ['honza', 'Jirka', 'Petr']};
            var document2 = {name: 'testDoc2', pole: ['honza', 'Ondra', 'Pavel']};
            pouchDBService.setDatabase(testDatabaseName)
                .then(function () {
                    return pouchDBService.saveDocument(document1);
                })
                .then(function () {
                    return pouchDBService.saveDocument(document2);
                })
                .then(function () {
                    var selector = {
                        _id: {$gt: null},
                        pole: {$elemMatch: {$eq: 'Ondra'}}
                    };
                    var query = {selector: selector};
                    return pouchDBService.customFind(query)
                        .then(function (data) {
                            console.info(data);
                            expect(data.docs.length).toBe(1);
                        })
                })
                .then(function () {
                    var selector = {
                        _id: {$gt: null},
                        pole: {$elemMatch: {$eq: 'honza'}}
                    };
                    var query = {selector: selector};
                    return pouchDBService.customFind(query)
                        .then(function (data) {
                            console.info(data);
                            expect(data.docs.length).toBe(2);
                            done();
                        })
                })
        })
    });

    describe('Get all docs from db', function () {

        it('should return doc', function (done) {

            var document1 = {name: 'testDoc1', pole: ['honza', 'Jirka', 'Petr']};
            var document2 = {name: 'testDoc2', pole: ['honza', 'Ondra', 'Pavel']};
            pouchDBService.setDatabase(testDatabaseName)
                .then(function () {
                    return pouchDBService.saveDocument(document1);
                })
                .then(function () {
                    return pouchDBService.saveDocument(document2);
                })
                .then(function () {
                    return pouchDBService.getAllDocs()
                        .then(function (data) {
                            console.info(data);
                            expect(data.total_rows).toBe(2);
                            expect(data.rows.length).toBe(2);
                            done();
                        })
                })
        });

        it('should return Object{total_rows:0, rows: []}', function (done) {
            pouchDBService.setDatabase(testDatabaseName)
                .then(function () {
                    return pouchDBService.getAllDocs()
                        .then(function (data) {
                            console.info(data);
                            expect(data.total_rows).toBe(0);
                            expect(data.rows).toEqual([]);
                            done();
                        })
                })
        })

        /*it('should failed with {type: pouchDBExceptions.FIND_EXCEPTION, message: \'Exception when finding documents\', cause: xxx} doc', function (done) {

         var document1 = {name: 'testDoc1', pole: ['honza', 'Jirka', 'Petr']};
         var document2 = {name: 'testDoc2', pole: ['honza', 'Ondra', 'Pavel']};
         pouchDBService.setDatabase(testDatabaseName)
         .then(function () {
         return pouchDBService.saveDocument(document1);
         })
         .then(function () {
         return pouchDBService.saveDocument(document2);
         })
         .then(function () {
         return pouchDBService.getAllDocs()
         .then(function (data) {
         console.info(data);
         expect(data.total_rows).toBe(2);
         done();
         })
         })
         })*/
    })


});
